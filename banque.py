""" je crée des classes !!! """

import csv
import os

operation: int


class Compte:
    """classe compte sers a voir le compte"""

    def __init__(self, numeroCompte, nomProprietaire, solde):
        self.numeroCompte = numeroCompte
        self.nomProprietaire = nomProprietaire
        self.solde = solde

    def retrait(self, operation):
        """ retire de l'argent du compte """
        if self.solde - operation < -400:
            print("opération impossible")
        else:
            self.solde = self.solde - operation
        print(self.solde)

    def versement(self, operation):
        """" ajoute de l'argent au compte """
        self.solde = self.solde + operation
        print(self.solde)

    def afficher_solde(self):
        """affiche la valeur du compte"""
        print(self.solde)

    def sauvegardefichier(self):
        """ savegarde dans un fichier les valeurs du compte """

        print(os.getcwd())  # Current directory : 'C:\\Demo'
        fichier = open("data.csv", "a")
        spamwriter = csv.writer(fichier, delimiter=';')
        spamwriter.writerow([self.solde, self.numeroCompte, self.nomProprietaire])
        fichier.close()


# child class
class CompteCourant(Compte):
    """ classe compte courrant sers a perdre des sous"""

    def __init__(self, NumeroCompte, nomProprietaire, solde, pourcentageAgios, autorisationDecouvert):
        super().__init__(NumeroCompte, nomProprietaire, solde)
        self.pourcentageAgios = pourcentageAgios
        self.autorisationDecouvert = autorisationDecouvert

    def appliquerAgios(self):
        """ crée des agios """
        self.solde = self.solde * (1 - self.pourcentageAgios)
        print(self.solde)


# child class
class CompteEpargne(Compte):
    """ classe copte epargne: sers a gagner des sous """

    def __init__(self, NumeroCompte, nomProprietaire, solde, pourcentageInterets):
        super().__init__(NumeroCompte, nomProprietaire, solde)
        self.pourcentageInterets = pourcentageInterets

    def appliquerInterets(self):
        """ ajoute un pourcentage d'interrets """
        self.pourcentageInterets = self.pourcentageInterets / 100
        self.solde = self.solde * (1 + self.pourcentageInterets)
        print(self.solde)
