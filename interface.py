from tkinter import *
import tkinter as tk
from tkinter import ttk
from ExerciceBanque.banque import *

CompteFloEpargne = CompteEpargne('fr743000000', 'giangrande', 14000, 14)
CompteFloCourant = CompteCourant('fr76000', 'giangrande', 1200, 12, 400)
operationCE = 0
operationCC = 0

root = tk.Tk()
root.geometry('1000x100')

p = PanedWindow(root, orient=HORIZONTAL)
p.pack(side=TOP, expand=Y, fill=BOTH, pady=1, padx=20)
p.add(Label(p, text='compte courant', background='white', anchor=CENTER))

p.add(Label(p, text="votre solde", background='pink', anchor=N))

solde_courant = Label(p, text=str(CompteFloCourant.solde), background='pink', anchor=N)
p.add(solde_courant)

def action(event):
    select = listeCombo.get()
    print("Vous avez sélectionné : '", select, "'")
    if select == "retrait":
        """renvoie a la fonction selectionne une valeur"""

        def fetch():
            montant = ent.get()
            print('Texte: <%s>' % ent.get())
            print(int(montant))
            CompteFloCourant.retrait(int(montant))

        ent = Entry(root)
        ent.insert(0, 'Montant')
        ent.pack(side=TOP, fill=X)
        ent.bind('<Return>', (lambda event: fetch()))
        b1 = Button(root, text='Fetch', command=fetch)
        b1.pack(side=LEFT)
        b2 = Button(root, text='Quit', command=root.destroy)
        b2.pack(side=RIGHT)

    if select == "virement":
        """revoie a la fonction selectionne une valeur"""
        def fetch():
            montant = ent.get()
            print('Texte: <%s>' % ent.get())
            print(int(montant))
            CompteFloCourant.versement(int(montant))

        ent = Entry(root)
        ent.insert(0, 'Montant')
        ent.pack(side=TOP, fill=X)
        ent.bind('<Return>', (lambda event: fetch()))
        b1 = Button(root, text='Fetch', command=fetch)
        b1.pack(side=LEFT)
        b2 = Button(root, text='Quit', command=root.destroy)
        b2.pack(side=RIGHT)


# 2) - créer la liste Python contenant les éléments de la liste Combobox
listeProduits = ["retrait", "virement"]

# 3) - Creation de la Combobox via la méthode ttk.Combobox()
listeCombo = ttk.Combobox(p, values=listeProduits)
p.add(listeCombo)
p.pack()
listeCombo.bind("<<ComboboxSelected>>", action)

p.add(Label(p, text='compte epargne', background='white', anchor=CENTER))
p.add(Label(p, text="votre solde", background='pink', anchor=N))
p.add(Label(p, text=str(CompteFloEpargne.solde), background='pink', anchor=N))
p.pack()


def action1(event):
    # Obtenir l'élément sélectionné
    select = listeCombo1.get()
    print("Vous avez sélectionné : '", select, "'")
    if select == "retrait":
        """renvoie a la fonction selectionne une valeur"""

        def fetch():
            montant = ent.get()
            print('Texte: <%s>' % ent.get())
            print(int(montant))
            CompteFloEpargne.retrait(int(montant))
        ent = Entry(root)
        ent.insert(0, 'Montant')
        ent.pack(side=TOP, fill=X)
        ent.bind('<Return>', (lambda event: fetch()))
        b1 = Button(root, text='Fetch', command=fetch)
        b1.pack(side=LEFT)
        b2 = Button(root, text='Quit', command=root.destroy)
        b2.pack(side=RIGHT)

    if select == "virement":
        """revoie a la fonction selectionne une valeur"""
        def fetch():
            montant = ent.get()
            print('Texte: <%s>' % ent.get())
            print(int(montant))
            CompteFloEpargne.versement(int(montant))
        ent = Entry(root)
        ent.insert(0, 'Montant')
        ent.pack(side=TOP, fill=X)
        ent.bind('<Return>', (lambda event: fetch()))
        b1 = Button(root, text='Fetch', command=fetch)
        b1.pack(side=LEFT)
        b2 = Button(root, text='Quit', command=root.destroy)
        b2.pack(side=RIGHT)


# 2) - créer la liste Python contenant les éléments de la liste Combobox
listeProduits1 = ["retrait", "virement"]

# 3) - Creation de la Combobox via la méthode ttk.Combobox()
listeCombo1 = ttk.Combobox(p, values=listeProduits1)
p.add(listeCombo1)
p.pack()
listeCombo1.bind("<<ComboboxSelected>>", action1)

p.add(Label(p, text='compte epargne', background='white', anchor=CENTER))
p.pack()

root.mainloop()

# bouton de sortie
bouton = Button(root, text="Fermer", command=root.quit)
bouton.pack()
root.mainloop()


"""

j'ai pas résussi a changer a valeur des labels mais la bonne valeur s'affiche en bas

"""