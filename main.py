"""
exercice cour
"""
from ExerciceBanque.banque import *


def pgcd(A: int, B: int):
    """ Création d'un PGCD """
    while A != B:
        if A < B:
            B = B - A
        if B < A:
            A = A - B
    return A


if __name__ == '__main__':

    """
    print(pgcd(144, 24))
    """
    comptefloEpargne = CompteEpargne('fr743000000', 'giangrande', 14000, 14)
    comptefloCourant = CompteCourant('fr76000', 'giangrande', 1200, 12, 400)
    operationCE = 0
    operationCC = 0
    choix = 4
    while (choix != 1 and choix != 2 and operationCE == 4 , operationCC == 4):
        try:
            choix = int(input("quelcompte voulez vous impacter ? \n1 - cc\n2 - ce\n"))
        except:
            "il y a eu un problème réessayez"
        try:
            if choix != 1 and choix != 2:
                print("retapez")
            if choix == 1:
                operationCC = int(input(
                    "souhaitez vous \n 1- voir votre solde \n 2-effectuer un virement \n 3- effectuer un retrait \n "
                    "4-quitter\n"))
                # comptefloCourant.sauvegardefichier()
                if operationCC == 1:
                    print("votre solde est de ", comptefloCourant.solde)
                if operationCC == 2:
                    montant = int(input("spécifiez le montant"))
                    comptefloCourant.versement(montant)
                    comptefloCourant.sauvegardefichier()
                if operationCC == 3:
                    montant1 = int(input("spécifiez le montant"))
                    comptefloCourant.retrait(montant1)
                    comptefloCourant.sauvegardefichier()
                    if comptefloCourant.value < 0:
                        comptefloCourant.appliquerAgios
                if operationCC == 4:
                    print("good bye")
                    break
                print("retapez")

            if choix == 2:

                operationCE = int(input(
                    "souhaitez vous \n 1- voir votre solde \n 2-effectuer un virement \n 3- effectuer un retrait \n 4-quitter\n"))
                # comptefloEpargne.sauvegardefichier()
                if operationCE == 1:
                    print("votre solde est de ", comptefloEpargne.solde)
                if operationCE == 2:
                    montant2 = int(input("spécifiez le montant"))
                    comptefloEpargne.versement(montant2)
                    comptefloEpargne.sauvegardefichier()
                if operationCE == 3:
                    montant3 = int(input("spécifiez le montant"))
                    comptefloEpargne.retrait(montant3)
                    comptefloEpargne.sauvegardefichier()
                if operationCE == 4:
                    print("goodbye")
                    break

                print("retapez")
        except:
            print("il y a eu un problem try réessaie")
